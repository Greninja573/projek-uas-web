window.onload = function () {
    let register = document.getElementById("register")

    function validateEmail(str){
        at = -1;
        dot = -1;
    
        for(i = 0; i< str.length; i++){
            if(str.charAt(i) == '@')
                at = i;
    
            if(str.charAt(i) == '.')
                dot = i;
        }
    
        console.log(at);
        console.log(dot);
    
        if(at == -1 || dot == -1)
            return false;
    
        if(str.substr(0, at) == "") return false;
    
        if(dot - at <= 1) return false;
        
        if(str.substr(dot, str.length) == "") return false;
        
        return true;
    }
    let loginvalidation = (e) => {
        let fullname = document.getElementById("fullname").value;
        let username = document.getElementById("username").value;
        let email = document.getElementById("email").value;
        let password = document.getElementById("password").value;
        let confirmpassword = document.getElementById("confirmpassword").value;

        e.preventDefault();

        if (fullname == '') {
            alert("Fullname must be Filled!");
        } else if (fullname.length < 8) {
            alert("Fullname must 8 Characters Minimal!");
        } else if (username == '') {
            alert("Username must be Filled!");
        } else if (username.length < 8) {
            alert("username must 8 Characters Minimal!")
        } else if (validateEmail(email) == false) {
            alert("Please Fill E-mail form with Right Value!");
        } else if (password == '') {
            alert("Password must be Filled!");
        } else if (password.length < 6) {
            alert("Password must 6 Characters Minimal!");
        } else if (password != confirmpassword) {
            alert("Password not Match!");
        } else {
            if (confirm("Are you Sure to Register?")) {
                alert("Welcome "+ username)
            } else {
                alert("Register Aborted!")
            }
        }
    }

    register.addEventListener("click", e => loginvalidation(e));
}