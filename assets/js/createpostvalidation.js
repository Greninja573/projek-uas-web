window.onload = function () {
    let submit = document.getElementById("submit");

    let validatefeedback = (e) => {
        let username = document.getElementById("username").value;
        let message = document.getElementById("message").value;

        e.preventDefault();

        if (username == '') {
            alert("Please fill your Username");
        } else if (username.length < 8) {
            alert("Username Minimal 8 Characters");
        } else if (message == '') {
            alert("Message must be Filled!");
        } else if (message.length < 20) {
            alert("Message must 20 Characters Minimal!")
        } else {
            alert("success!")
        }
    }

    submit.addEventListener("click", e => validatefeedback(e))
}