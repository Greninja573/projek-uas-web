window.onload = function () {
    let login = document.getElementById("login")
    let loginvalidation = (e) => {
        let username = document.getElementById("username").value;
        let password = document.getElementById("password").value;

        e.preventDefault();

        if (username == '') {
            alert("Username must be Filled!");
        } else if (username.length < 8) {
            alert("Username must 8 Characters Minimal!");
        } else if (password == '') {
            alert("Password must be Filled!");
        } else {
            alert("Welcome " + username);
        }
    }

    login.addEventListener("click", e => loginvalidation(e));
}